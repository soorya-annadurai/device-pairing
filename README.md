The Playful Device Pairing
===

Team members:
    Bhargav Bhatkalkar,
    Soorya Annadurai

Assumption - client always makes the first contact to the server.

Methodology:
---
####Version 1 - Two- Device pairing using 2 runs####
Two devices ‘A’ and ‘B’ can be paired by playing a matching game on both of them. The public keys of device 
‘A’ and ‘B’ are first exchanged using insecure wireless medium (eg. Bluetooth). Then, a hash value for the
public key of each device is computed and then mapped to a 4*4 table of alphabets on both the devices.

The length of the hash value generated is 64-bits.The hash value of 64-bits is divided into 16 sets of 4-bit
each. Now, each 4-bit set is mapped to an equivalent alphabet as shown below:
Let the 64-bit hash value (Out-Of-Band string (OOB)) for public key of device ‘A’ be
1100 0101 1110 0111 0011 1100 0101 1010 1111 0000 1011 1010 1010 0101 1010 1110
Mapping of 4-bits to one of the 16 alphabets is as shown below:
0000 – A
0001 – B
0010 – C
0011 – D
0100 - E
0101 - F
0110 - G
0111 - H
1000 - I
1001 - J
1010 - K
1011 - L
1100 - M
1101 - N
1110 - O
1111 - P
The table mapped to the above OOB string at device ‘A’ is :
M
D
P
K
F
M
A
F
O
F
L
K
H
K
K
O
The above mapped table is then displayed on device ‘A’. Now on device ‘B’, an empty 4*4 table is displayed
whose cells has to be filled by the user exactly as it is in the mapped table of device ‘A’. The user can use
buttons on the devices to make the entry in the table.
The device ‘B’ will calculate the hash value for the public-key of device ‘A’ that it has received using wireless
channel. Since a universal hash function is used, the length of this hash will also be 64-bits.
As the user starts filling the empty table, a comparison is made between every 4-bits of hash value already
computed and the 4-bits associated to the alphabet entry made in the empty cell.
For example, at device ‘B’, the computed hash value for the public-key of device ‘A’ received through
wireless channel is (assuming no modification during the transit):
1100 0101 1110 0111 0011 1100 0101 1010 1111 0000 1011 1010 1010 0101 1010 1110
When the first entry (M) in the empty table is made, the 4-bits associated with the ‘M’ (1100) are compared
with the first 4-bits of the hash value (1100). If there a proper match, the next entry ‘F’ is allowed to make
and once again its associated 4-bits (0101) are compared with the second set of 4-bits of the hash value
(0101). The process will continue till all the entries in the table are made and the associated bits are matchedwith respective 4-bit sets in the hash value. Now the device ‘B’ can conclude that it has received the
legitimate public key of device ’A’. The empty table is always filled from left-to-right, top-to-bottom.
M
F
To authorize the public key of device ‘B’ at device ‘A’, the entire above procedure is run again by displaying
the mapped table at device ‘B’ and then filling its entries in the empty table at device ‘A’.
Now both the devices are paired and after analyzing I found that man-in-the-middle-attack (MITM) is not
possible with this approach if we use pre-image resistance hash function (Formal proof needs to be derived).