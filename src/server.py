import logging
import socket
from include.config.network.server import SERVER_IP_ADDRESS, SERVER_PORT
from include.security import security
from include.config.security import server


logging.basicConfig(format='%(levelname)s: %(asctime)s %(message)s',
					datefmt='%m/%d/%Y %I:%M:%S %p',
					level=logging.FATAL)

SERVER_PUBLIC_KEY = server.SERVER_PUBLIC_KEY

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (SERVER_IP_ADDRESS, SERVER_PORT)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)
sock.listen(5)
participant_count = int(raw_input("Enter number of participants: "))
i = 0
client_connections = []
client_public_keys = []
while i < participant_count:
	print "Waiting for connection"
	client_connection, client_address = sock.accept()
	client_connections.append(client_connection)
	client_connection.sendall(str(participant_count))
	logging.info("Sent participant count: " + str(participant_count))
	client_public_key = client_connection.recv(64)
	logging.info("Received client's public key: " + client_public_key)
	client_public_keys.append(client_public_key)
	i += 1
try:
	peepholes = security.get_randomized_peepholes(16, participant_count)
	i = 0
	for client_connection in client_connections:
		for public_key in client_public_keys:
			client_connection.sendall(public_key)
			client_connection.recv(16)  # Receive an acknowledgement, just to ensure the buffer is flushed.
		peephole_str = ','.join(list(map(str, peepholes[i])))
		logging.debug("Client #" + str(i) + ", peephole: <" + peephole_str + ">")
		client_connection.sendall(peephole_str)
		client_connection.recv(16)  # Receive an acknowledgement, just to ensure the buffer is flushed.
		i += 1

	logging.info("Server coordination task complete")
	print "Server coordination task complete"
except AssertionError as e:
	logging.fatal(e.message)

for client_connection in client_connections:
	client_connection.close()
