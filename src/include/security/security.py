import logging
import hashlib
import random
import math


def get_hash(input_string):
	m = hashlib.sha512()
	m.update(input_string)
	return m.hexdigest()


def merge_keys(keys):
	return "".join(str(key) for key in keys)


def get_randomized_peepholes(range_max, participant_count):
	shuffled_sequence = range(range_max)
	random.shuffle(shuffled_sequence)
	peepholes = []
	peephole_size_per_client = math.ceil(float(len(shuffled_sequence)) / float(participant_count))
	for i in range(participant_count):
		peepholes.append([])
	logging.info("Number of peepholes: " + str(len(peepholes)))
	for i in range(len(shuffled_sequence)):
		peepholes[i % len(peepholes)].append(shuffled_sequence[i])
	logging.info("Shuffled sequence: " + str(shuffled_sequence))
	logging.info("Peephole size per client: " + str(peephole_size_per_client))
	return peepholes
