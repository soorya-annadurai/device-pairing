import logging


def letter_from_hex(letter):
	if letter == '0':
		return 'A'
	if letter == '1':
		return 'B'
	if letter == '2':
		return 'C'
	if letter == '3':
		return 'D'
	if letter == '4':
		return 'E'
	if letter == '5':
		return 'F'
	if letter == '6':
		return 'G'
	if letter == '7':
		return 'H'
	if letter == '8':
		return 'I'
	if letter == '9':
		return 'J'
	if letter == 'a':
		return 'K'
	if letter == 'b':
		return 'L'
	if letter == 'c':
		return 'M'
	if letter == 'd':
		return 'N'
	if letter == 'e':
		return 'O'
	if letter == 'f':
		return 'P'
	return letter


def get_last_16_chars_of_string(original):
	if len(original) <= 16:
		return original
	return original[-16:]


def swap_hex_for_letter(original):
	mod_original = []
	for i in range(len(original)):
		mod_original.append(letter_from_hex(original[i]))
	return mod_original


def swap_hex_string_for_letter(original):
	return str("".join(swap_hex_for_letter(original)))


def merge_strings(base, new, delimiter=" "):
	for i in range(len(new)):
		base = base.replace(delimiter, str(new[i]), 1)
	return base


def display_matrix(hash16, debug_mode=False):
	if hash16 is None:
		return
	print "Displaying matrix: "
	resultant_string = []
	for i in range(4):
		for j in range(4):
			if (i * 4 + j) >= len(hash16):
				i = 4
				break
			if debug_mode is False:
				print letter_from_hex(hash16[i * 4 + j]) + " ",
			resultant_string.append(letter_from_hex(hash16[i * 4 + j]))
		if debug_mode is False:
			print "\n",
	if debug_mode is False:
		print "\n",
	if debug_mode is True:
		print "".join(resultant_string)


def get_matrix_to_display(input_string, peephole, mode="client"):
	if input_string is None:
		return
	if len(input_string) < 16:
		return
	base_string = input_string[-16:]
	answer = ""
	for i in range(len(base_string)):
		if i in peephole:
			answer += base_string[i]
		else:
			answer += " "
	if mode == "universal":
		return base_string
	else:
		return answer
