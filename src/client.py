import logging
import socket
from include.alphamap.alphamap import swap_hex_string_for_letter, merge_strings, display_matrix, get_matrix_to_display
from include.config.network.server import SERVER_IP_ADDRESS, SERVER_PORT
from include.config.security import client
from include.security import security
from include.config.network import constants

logging.basicConfig(format='%(levelname)s: %(asctime)s %(message)s',
					datefmt='%m/%d/%Y %I:%M:%S %p',
					level=logging.FATAL)

CLIENT_PUBLIC_KEY = client.CLIENT_PUBLIC_KEY

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (SERVER_IP_ADDRESS, SERVER_PORT)
sock.connect(server_address)

try:
	participant_count = int(sock.recv(16))
	sock.sendall(CLIENT_PUBLIC_KEY)
	logging.info("Sent client's public key: " + CLIENT_PUBLIC_KEY)

	public_keys = []
	for i in range(participant_count):
		public_key = sock.recv(64)
		public_keys.append(public_key)
		sock.sendall(constants.ACK)
	peephole_str = sock.recv(64)
	sock.sendall(constants.ACK)
	logging.debug("Mask_str: <" + peephole_str + ">")
	peephole = list(map(int, peephole_str.rstrip(',').split(',')))
	peephole.sort()
	# TODO: Utilize sorted property of peephole values.

	hashed_keys = security.get_hash(security.merge_keys(public_keys))
	display_string = get_matrix_to_display(hashed_keys, peephole)
	hash_for_client = get_matrix_to_display(hashed_keys, peephole)
	true_answer = get_matrix_to_display(hashed_keys, peephole, "universal")
	display_matrix(display_string, debug_mode=False)
	oob__server_string = raw_input("Enter the missing symbols: ")
	assert swap_hex_string_for_letter(true_answer) \
		== merge_strings(swap_hex_string_for_letter(hash_for_client), str(oob__server_string)), \
		"Out-of-bound server entry is incorrect"
	logging.info("Server successfully authenticated")
	print "Authentication process successful"
except AssertionError as e:
	logging.fatal(e.message)
